/**
 * @file
 * @brief Defines a user hook for Geant4 to assign custom step information via G4Step objects.
 * @copyright Copyright (c) 2017-2020 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#ifndef SetUserSteppingActionG4_H
#define SetUserSteppingActionG4_H 1

#include "G4Track.hh"
#include "G4UserSteppingAction.hh"
#include "objects/MCStep.hpp"

#include "TrackInfoManager.hpp"
#include "core/messenger/Messenger.hpp"

namespace allpix {
    /**
     * @brief Assigns every step of each particle an MCStep which carries various inforamtion
     */
    class SetUserSteppingActionG4 : public G4UserSteppingAction {
    public:
        /**
         * @brief Constructor
         * @param module calls the to dispatch the message from this module
         * @param config to access the configuration file
         * @param mgs Messenger* to create and send a MCSTep message
         * @param track_info_manager reference to the track info manager
         */
        explicit SetUserSteppingActionG4(Module* module,
                                         const Configuration& config,
                                         Messenger* msg,
                                         TrackInfoManager* track_info_manager);

        /**
         * @brief Default destructor
         */
        ~SetUserSteppingActionG4() override = default;

        /**
         * @brief Saves information for every G4Step of each particle in each event
         * @param aStep The pointer to the G4Step for which this routine is called
         */
        virtual void UserSteppingAction(const G4Step* aStep) override;
        /**
         * @brief Dispatches the step information for each saved step
         */
        void dispatchMessages();

    private:
        Module* module_;
        const Configuration& config_;
        Messenger* messenger_;
        TrackInfoManager* track_info_manager_;

        // A vector of the steps to be saved
        std::vector<MCStep> steps_;
        // Integer to keep track of the event number
        int event_id_;
    };

} // namespace allpix
#endif /* SetUserSteppingActionG4_H */
