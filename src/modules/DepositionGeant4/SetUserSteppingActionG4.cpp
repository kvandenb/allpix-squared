#include "SetUserSteppingActionG4.hpp"
#include "G4Event.hh"
#include "G4Neutron.hh"
#include "G4RunManager.hh"
#include "TrackInfoG4.hpp"

using namespace allpix;

SetUserSteppingActionG4::SetUserSteppingActionG4(Module* module,
                                                 const Configuration& config,
                                                 Messenger* msg,
                                                 TrackInfoManager* track_info_manager)
    : module_(module), config_(config), messenger_(msg), track_info_manager_(track_info_manager) {
    event_id_ = 1;
}

void SetUserSteppingActionG4::UserSteppingAction(const G4Step* aStep) {
    if(config_.get<bool>("track_steps", false)) {
        // Track the event_id
        auto event_id_min = config_.get<int>("event_id_min", 1);
        auto event_id_max = config_.get<int>("event_id_max", INT_MAX);
        if(event_id_ < event_id_min || event_id_ > event_id_max) {
            return;
        }

        // Track the track_id
        auto track = aStep->GetTrack();
        auto track_id = track->GetTrackID();
        auto track_id_min = config_.get<int>("track_id_min", 1);
        auto track_id_max = config_.get<int>("track_id_max", INT_MAX);
        if(track_id < track_id_min || track_id > track_id_max) {
            return;
        }

        auto step_number = track->GetCurrentStepNumber();
        auto particle_id = track->GetParticleDefinition()->GetPDGEncoding();
        if(track->GetParticleDefinition() != G4Neutron::Definition()) {
            return;
        }

        auto pre_step = aStep->GetPreStepPoint();
        auto post_step = aStep->GetPostStepPoint();
        auto initial_position = static_cast<ROOT::Math::XYZPoint>(pre_step->GetPosition());
        auto initial_energy = pre_step->GetKineticEnergy();

        // Filter by initial energy
        auto minimal_step_energy = config_.get<double>("minimal_step_energy", 0);
        if(initial_energy < minimal_step_energy) {
            return;
        }
        auto initial_momentum = static_cast<ROOT::Math::XYZVector>(pre_step->GetMomentum());
        auto final_position = static_cast<ROOT::Math::XYZPoint>(post_step->GetPosition());
        auto final_energy = post_step->GetKineticEnergy();
        auto final_momentum = static_cast<ROOT::Math::XYZVector>(post_step->GetMomentum());
        steps_.emplace_back(event_id_,
                            track_id,
                            step_number,
                            particle_id,
                            initial_position,
                            final_position,
                            initial_momentum,
                            final_momentum,
                            initial_energy,
                            final_energy);
    }
}

void SetUserSteppingActionG4::dispatchMessages() {
    if(!steps_.empty()) {
        // Match TrackID to MCTrack
        for(auto& step : steps_) {
            auto id = step.getTrackID();
            if(track_info_manager_->findMCTrack(id) != nullptr) {
                step.setTrack(track_info_manager_->findMCTrack(id));
            }
        }
        // Send the mc particle information
        auto mc_step_message = std::make_shared<MCStepMessage>(std::move(steps_));
        messenger_->dispatchMessage(module_, mc_step_message);
    }
    // clear steps for next event
    steps_ = std::vector<MCStep>();
    event_id_++;
}
