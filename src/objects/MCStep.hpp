/**
 * @file
 * @brief Definition of Monte-Carlo track object
 * @copyright Copyright (c) 2018-2019 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#ifndef ALLPIX_MC_STEP_H
#define ALLPIX_MC_STEP_H

#include <Math/Point3D.h>
#include <Math/Vector3D.h>
#include <TRef.h>

#include "Object.hpp"
#include "MCTrack.hpp"

namespace allpix {
    /**
     * @brief Monte-Carlo track through the world
     */
    class MCStep : public Object {
    public:
        /**
         * @brief Construct a Monte-Carlo track
         * @param start_point Global point where track came into existance
         * @param end_point Global point where track went out of existance
         * @param g4_volume Geant4 volume where track originated in
         * @param g4_prod_process_name Geant4 creation process name
         * @param g4_prod_process_type Geant4 creation process id
         * @param particle_id PDG particle id
         * @param initial_kin_E Initial kinetic energy (in MeV)
         * @param final_kin_E Final kinetic energy (in MeV)
         * @param initial_tot_E Initial total energy (in MeV)
         * @param final_tot_E Final total energy (in MeV)
         */
        MCStep(int event_id,
               int track_id,
               int step_number,
               int particle_id,
               ROOT::Math::XYZPoint start_point,
               ROOT::Math::XYZPoint end_point,
               ROOT::Math::XYZVector initial_momentum,
               ROOT::Math::XYZVector final_momentum,
               double initial_kin_E_,
               double final_kin_E_);

        int getEventID() const;
        int getTrackID() const;
        int getParticleID() const;
        int getStepNumber() const;
        ROOT::Math::XYZPoint getStartPoint() const;
        ROOT::Math::XYZPoint getEndPoint() const;
        ROOT::Math::XYZVector getInitialMomentum() const;
        ROOT::Math::XYZVector getFinalMomentum() const;
        double getInitialKinE() const;
        double getFinalKinE() const;

        /**
         * @brief Set the MCStep's track
         * @param mc_track The track
         * @warning Special method because track can only be set after creation, should not be replaced later.
         */
        void setTrack(const MCTrack* mc_track);

        /**
         * @brief Get the MCTrack of this MCStep
         * @return Parent MCTrack or null pointer if it has no track
         * @warning No \ref MissingReferenceException is thrown, because a step without a track should always be handled.
         */
        const MCTrack* getTrack() const;

        /**
         * @brief Print an ASCII representation of MCTrack to the given stream
         * @param out Stream to print to
         */
        void print(std::ostream& out) const override;

        /**
         * @brief ROOT class definition
         */
        ClassDefOverride(MCStep, 2);
        /**
         * @brief Default constructor for ROOT I/O
         */
        MCStep() = default;

    private:
        int event_id_{};
        int track_id_{};
        int step_number_{};
        int particle_id_{};
        ROOT::Math::XYZPoint start_point_{};
        ROOT::Math::XYZPoint end_point_{};
        ROOT::Math::XYZVector initial_momentum_{};
        ROOT::Math::XYZVector final_momentum_{};
        double initial_kin_E_;
        double final_kin_E_;

        TRef track_;
    };

    /**
     * @brief Typedef for message carrying MC tracks
     */
    using MCStepMessage = Message<MCStep>;
} // namespace allpix

#endif /* ALLPIX_MC_STEP_H */
