/**
 * @file
 * @brief Implementation of Monte-Carlo track object
 * @copyright Copyright (c) 2018-2019 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#include "MCStep.hpp"
#include <sstream>

using namespace allpix;

MCStep::MCStep(int event_id,
               int track_id,
               int step_number,
               int particle_id,
               ROOT::Math::XYZPoint start_point,
               ROOT::Math::XYZPoint end_point,
               ROOT::Math::XYZVector initial_momentum,
               ROOT::Math::XYZVector final_momentum,
               double initial_kin_E,
               double final_kin_E)
    : event_id_(event_id), track_id_(track_id), step_number_(step_number), particle_id_(particle_id),
      start_point_(std::move(start_point)), end_point_(std::move(end_point)), initial_momentum_(std::move(initial_momentum)),
      final_momentum_(std::move(final_momentum)), initial_kin_E_(initial_kin_E), final_kin_E_(final_kin_E) {
    setTrack(nullptr);
}

ROOT::Math::XYZVector MCStep::getInitialMomentum() const {
    return initial_momentum_;
}

ROOT::Math::XYZVector MCStep::getFinalMomentum() const {
    return final_momentum_;
}

ROOT::Math::XYZPoint MCStep::getStartPoint() const {
    return start_point_;
}

ROOT::Math::XYZPoint MCStep::getEndPoint() const {
    return end_point_;
}

int MCStep::getEventID() const {
    return event_id_;
}
int MCStep::getTrackID() const {
    return track_id_;
}
int MCStep::getParticleID() const {
    return particle_id_;
}
int MCStep::getStepNumber() const {
    return step_number_;
}
double MCStep::getInitialKinE() const {
    return initial_kin_E_;
}
double MCStep::getFinalKinE() const {
    return final_kin_E_;
}

/**
 * Object is stored as TRef and can only be accessed if pointed object is in scope
 */
void MCStep::setTrack(const MCTrack* mc_track) {
    track_ = const_cast<MCTrack*>(mc_track); // NOLINT
}

/**
 * Object is stored as TRef and can only be accessed if pointed object is in scope
 */
const MCTrack* MCStep::getTrack() const {
    return dynamic_cast<MCTrack*>(track_.GetObject());
}

void MCStep::print(std::ostream& out) const {
    static const size_t big_gap = 25;
    static const size_t med_gap = 10;
    static const size_t small_gap = 6;
    static const size_t largest_output = 2 * big_gap + 2 * med_gap + 2 * small_gap;

    auto title = std::stringstream();
    title << "--- Printing MCStep information for track (" << this << ") ";

    out << "Upcoming" << std::endl
        << std::setw(largest_output) << std::left << std::setfill('-') << title.str() << '\n'
        << std::setfill(' ') << std::left << std::setw(big_gap) << "Particle type (PDG ID): " << std::right
        << std::setw(small_gap) << particle_id_ << '\n'
        << std::left << std::setw(big_gap) << "Initial position:" << std::right << std::setw(med_gap) << start_point_.X()
        << " mm |" << std::setw(med_gap) << start_point_.Y() << " mm |" << std::setw(med_gap) << start_point_.Z() << "mm\n"
        << std::left << std::setw(big_gap) << "Final position:" << std::right << std::setw(med_gap) << end_point_.X()
        << " mm |" << std::setw(med_gap) << end_point_.Y() << " mm |" << std::setw(med_gap) << end_point_.Z() << " mm\n"
        << std::left << std::setw(big_gap) << "Initial kinetic energy: " << std::right << std::setw(med_gap)
        << initial_kin_E_ << std::setw(small_gap) << " MeV | " << std::left << std::setw(big_gap)
        << "Final kinetic energy: " << std::right << std::setw(med_gap) << final_kin_E_ << std::setw(small_gap);

    out << std::setfill('-') << std::setw(largest_output) << "" << std::setfill(' ') << std::endl;
}

ClassImp(MCStep)
